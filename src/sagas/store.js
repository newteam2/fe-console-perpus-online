import { createStore, combineReducers, applyMiddleware } from 'redux';
import logger from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import rootSaga from './rootSaga';
import bookReducer from '../reducers/bookReducer';

const appReducer = combineReducers({
    book: bookReducer,
});

const rootReducers = (state, action) => {
    return appReducer(state, action)
}

const sagaMiddleware = createSagaMiddleware();
const middleware = applyMiddleware(sagaMiddleware, logger);
const store = createStore(rootReducers, middleware);

sagaMiddleware.run(rootSaga);
export default store;
