
import { put, takeLatest } from 'redux-saga/effects';
import axios from 'axios';
import {
    GetListBook,
    GetListBookStart,
    GetListBookSuccess,
    GetListBookFailed,
    AddBook,
    AddBookStart,
    AddBookSuccess,
    AddBookFailed,
    DeleteBook,
    DeleteBookStart,
    DeleteBookSuccess,
    DeleteBookFailed,
    UploadImage,
    UploadImageStart,
    UploadImageSuccess,
    UploadImageFailed,
    UploadFile,
    UploadFileStart,
    UploadFileSuccess,
    UploadFileFailed,
    UpdateBook,
    UpdateBookStart,
    UpdateBookSuccess,
    UpdateBookFailed
} from '../config/actionType';
import {
    GetListBookApi,
    AddBookApi,
    DeleteBookApi,
    UploadImageApi,
    UploadFileApi,
    CrudBookApi
} from '../config/api';

function* getListBook() {
    yield put({ type: GetListBookStart });
    try {
        const res = yield axios.get(GetListBookApi);
        yield put({ type: GetListBookSuccess, data: res.data.data.rows });
    } catch (error) {
        yield put({ type: GetListBookFailed });
    }

}

function* addBook({ value }) {
    yield put({ type: AddBookStart });
    try {
        const res = yield axios.post(AddBookApi, value);
        yield put({ type: AddBookSuccess });
    } catch (error) {
        yield put({ type: AddBookFailed });
    }
}

function* updateBook({ value }) {
    yield put({ type: UpdateBookStart });
    try {
        const res = yield axios.put(`${CrudBookApi}/${value.id}`, value.body);
        yield put({ type: UpdateBookSuccess, data: res.data.data });
    } catch (error) {
        yield put({ type: UpdateBookFailed });
    }
}

function* deleteBook({ value }) {
    yield put({ type: DeleteBookStart });
    try {
        const res = yield axios.delete(`${DeleteBookApi}/${value}`, {
            data: { deleted: 1 }
        });
        yield put({ type: DeleteBookSuccess });
    } catch (error) {
        yield put({ type: DeleteBookFailed });
    }
}

function* uploadImage({ value }) {
    yield put({ type: UploadImageStart });
    try {
        const res = yield axios.post(UploadImageApi, value);
        yield put({ type: UploadImageSuccess, data: res.data.data.url });
    } catch (error) {
        yield put({ type: UploadImageFailed });
    }
}

function* uploadFile({ value }) {
    yield put({ type: UploadFileStart });
    try {
        const res = yield axios.post(UploadFileApi, value);
        yield put({ type: UploadFileSuccess, data: res.data.data.url });
    } catch (error) {
        yield put({ type: UploadFileFailed });
    }
}

export function* bookWatcher() {
    yield takeLatest(GetListBook, getListBook);
    yield takeLatest(AddBook, addBook);
    yield takeLatest(UpdateBook, updateBook);
    yield takeLatest(DeleteBook, deleteBook);
    yield takeLatest(UploadImage, uploadImage);
    yield takeLatest(UploadFile, uploadFile);
}   