import { all } from 'redux-saga/effects';
import { bookWatcher } from './bookSaga';

const watchers = [
    bookWatcher(),
];

export default function* rootSaga() {
    yield all(watchers);
}