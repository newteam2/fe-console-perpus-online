// Book //
// Get list book
export const GetListBook = 'GET_LIST_BOOK'
export const GetListBookStart = 'GET_LIST_BOOK_START'
export const GetListBookSuccess = 'GET_LIST_BOOK_SUCCESS'
export const GetListBookFailed = 'GET_LIST_BOOK_FAILED'

// Add book
export const AddBook = 'ADD_BOOK'
export const AddBookStart = 'ADD_BOOK_START'
export const AddBookSuccess = 'ADD_BOOK_SUCCESS'
export const AddBookFailed = 'ADD_BOOK_FAILED'

// Update book
export const UpdateBook = 'UPDATE_BOOK'
export const UpdateBookStart = 'UPDATE_BOOK_START'
export const UpdateBookSuccess = 'UPDATE_BOOK_SUCCESS'
export const UpdateBookFailed = 'UPDATE_BOOK_FAILED'

// Delete book
export const DeleteBook = 'DELETE_BOOK'
export const DeleteBookStart = 'DELETE_BOOK_START'
export const DeleteBookSuccess = 'DELETE_BOOK_SUCCESS'
export const DeleteBookFailed = 'DELETE_BOOK_FAILED'

// Upload image
export const UploadImage = 'UPLOAD_IMAGE'
export const UploadImageStart = 'UPLOAD_IMAGE_START'
export const UploadImageSuccess = 'UPLOAD_IMAGE_SUCCESS'
export const UploadImageFailed = 'UPLOAD_IMAGE_FAILED'

// Upload file
export const UploadFile = 'UPLOAD_FILE'
export const UploadFileStart = 'UPLOAD_FILE_START'
export const UploadFileSuccess = 'UPLOAD_FILE_SUCCESS'
export const UploadFileFailed = 'UPLOAD_FILE_FAILED'