const BaseUrl = 'https://perpus-online-new.herokuapp.com'

// Book
export const GetListBookApi = `${BaseUrl}/books?search=&page=1&limit=6&sort=desc`
export const AddBookApi = `${BaseUrl}/books`
export const CrudBookApi = `${BaseUrl}/books`
export const DeleteBookApi = `${BaseUrl}/books`
export const UploadImageApi = `${BaseUrl}/upload/image`
export const UploadFileApi = `${BaseUrl}/upload/file`