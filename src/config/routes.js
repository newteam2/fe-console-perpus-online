// Book
export const ListBook = '/'
export const ViewBookDetail = '/book'
export const ViewBookDetailId = '/book/:id'
export const AddNewBook = '/book/add'
export const UpdateBook = '/book/update'
export const UpdateBookId = '/book/update/:id'