import BackgroundView from '../assets/image/backgroundView.png'
import Logo from '../assets/image/logo.png'
import Like from '../assets/image/like.png'
import DisLike from '../assets/image/dislike.png'
import Add from '../assets/image/add.png'
import Eye from '../assets/image/eye.png'
import Pencil from '../assets/image/pencil.png'
import Trash from '../assets/image/trash.png'
import Sort from '../assets/image/sort.png'
import Menu from '../assets/image/menu.png'

export {
    BackgroundView,
    Logo,
    Like,
    DisLike,
    Add,
    Eye,
    Pencil,
    Trash,
    Sort,
    Menu
}