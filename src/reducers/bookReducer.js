import {
    GetListBookStart,
    GetListBookSuccess,
    GetListBookFailed,
    AddBookStart,
    AddBookSuccess,
    AddBookFailed,
    DeleteBookStart,
    DeleteBookSuccess,
    DeleteBookFailed,
    UploadImageStart,
    UploadImageSuccess,
    UploadImageFailed,
    UploadFileStart,
    UploadFileSuccess,
    UploadFileFailed,
    UpdateBookStart,
    UpdateBookSuccess,
    UpdateBookFailed
} from '../config/actionType';

const initialState = {
    isLoading: false,
    isSuccess: false,
    isError: false,
    list: [],
    fetchDelete: false,
    isDeleting: false,
    urlImage: '',
    urlFile: '',
    isLoadingUpImg: false,
    isSuccessUpImg: false,
    isErrorUpImg: false,
    isLoadingUpFile: false,
    isSuccessUpFile: false,
    isErrorUpFile: false,
    isSuccessAdd: false,
    isErrorAdd: false,
    isSuccessUpdate: false,
    isErrorUpdate: false,
    bookDetail: null
}

const bookReducer = (state = initialState, action) => {
    switch (action.type) {
        case GetListBookStart:
            return {
                ...state,
                isLoading: true,
                isSuccess: false,
                isError: false,
                isSuccessAdd: false,
                isErrorAdd: false,
                isSuccessUpImg: false,
                isErrorUpImg: false,
                isSuccessUpFile: false,
                isErrorUpFile: false,
                isSuccessUpdate: false,
                isErrorUpdate: false
            }
        case GetListBookSuccess:
            return {
                ...state,
                isLoading: false,
                isSuccess: true,
                isDeleting: false,
                list: action.data,
            }
        case GetListBookFailed:
            return {
                ...state,
                isLoading: false,
                isError: true,
            }
        case AddBookStart:
            return {
                ...state,
                isLoading: true,
                isSuccessAdd: false,
                isErrorAdd: false
            }
        case AddBookSuccess:
            return {
                ...state,
                isLoading: false,
                isSuccessAdd: true,
            }
        case AddBookFailed:
            return {
                ...state,
                isLoading: false,
                isErrorAdd: true,
            }
        case UpdateBookStart:
            return {
                ...state,
                isLoading: true,
                isSuccessUpdate: false,
                isErrorUpdate: false,
                bookDetail: null,
                isSuccessUpImg: false,
                isErrorUpImg: false,
                isSuccessUpFile: false,
                isErrorUpFile: false,
            }
        case UpdateBookSuccess:
            return {
                ...state,
                isLoading: false,
                isSuccessUpdate: true,
                bookDetail: action.data
            }
        case UpdateBookFailed:
            return {
                ...state,
                isLoading: false,
                isErrorUpdate: true,
            }
        case DeleteBookStart:
            return {
                ...state,
                isLoading: true,
                isDeleting: false,
                isError: false
            }
        case DeleteBookSuccess:
            return {
                ...state,
                isLoading: false,
                fetchDelete: !state.fetchDelete,
                isDeleting: true,
            }
        case DeleteBookFailed:
            return {
                ...state,
                isLoading: false,
                isError: true,
            }
        case UploadImageStart:
            return {
                ...state,
                isLoadingUpImg: true,
                isSuccessUpImg: false,
                isErrorUpImg: false
            }
        case UploadImageSuccess:
            return {
                ...state,
                isLoadingUpImg: false,
                isSuccessUpImg: true,
                urlImage: action.data,
            }
        case UploadImageFailed:
            return {
                ...state,
                isLoadingUpImg: false,
                isErrorUpImg: true,
            }
        case UploadFileStart:
            return {
                ...state,
                isLoadingUpFile: true,
                isSuccessUpFile: false,
                isErrorUpFile: false
            }
        case UploadFileSuccess:
            return {
                ...state,
                isLoadingUpFile: false,
                isSuccessUpFile: true,
                urlFile: action.data,
            }
        case UploadFileFailed:
            return {
                ...state,
                isLoadingUpFile: false,
                isErrorUpFile: true,
            }
        default:
            return state
    }
}

export default bookReducer
