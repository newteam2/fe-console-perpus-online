import React from 'react';
import { Link } from 'react-router-dom';
import {
    ViewBookDetail,
    UpdateBook
} from '../../../config/routes';
import Loader from '../../Loader';
import './index.css';
import { Eye, Pencil, Trash } from '../../../config/images';
import moment from 'moment';

function Table(props) {
    const header = [
        { name: 'Image', icon: '', style: 'h-image' },
        { name: 'No', icon: '', style: 'no' },
        { name: 'Title', icon: '', style: 'title' },
        { name: 'Description', icon: '', style: 'desc' },
        { name: 'Publisher', icon: '', style: 'pub' },
        { name: 'Publish Date', icon: '', style: 'pubdate' },
    ]

    const { showMenu, data, isLoading, isError, isDeleting, handleDelete } = props
    return (
        <div className="book-main-content">
            <div className={showMenu ? "table-container active" : "table-container"}>
                <div className="table-row-header">
                    <div className="table-header data">
                        {header.map((item, index) => {
                            return (
                                <div key={index} className={`content-item ${item.style}`}>
                                    <p className={item.style == 'pub' || item.style == 'pubdate' ? "item-name white" : "item-name"}>{item.name}</p>
                                    {item.icon ?
                                        <button>
                                            <image />
                                        </button>
                                        : null
                                    }
                                </div>
                            )
                        })}
                    </div>
                    <div className="table-header action">
                        <div className="content-item action">
                            <p className="item-name white">Action</p>
                        </div>
                    </div>
                </div>
                <Loader isLoading={isLoading && !isDeleting} showMenu={showMenu} />
                {
                    data.map((item, index) => {
                        return (
                            <div key={index} className="table-row-content">
                                <div className="table-content data" >
                                    <div className="content-item image">
                                        <img className="item-image" src={item.book_image ? item.book_image : "https://inixindojogja.co.id/wp-content/uploads/2020/03/react.png.webp"} />
                                    </div>
                                    <div className="content-item no">
                                        <p className="item-name">{item.book_isbn}</p>
                                    </div>
                                    <div className="content-item title">
                                        <p className="item-name title">{item.book_name}</p>
                                    </div>
                                    <div className="content-item desc">
                                        <p className="item-name desc">{item.book_description || "-"}</p>
                                    </div>
                                    <div className="content-item pub">
                                        <p className="item-name pub">{item.publisher}</p>
                                    </div>
                                    <div className="content-item pubdate">
                                        <p className="item-name">{moment(item.published_date).format("ll")}</p>
                                    </div>
                                </ div>
                                < div className="table-content action" >
                                    <ActionBtn
                                        id={item.id}
                                        showMenu={showMenu}
                                        item={item}
                                        handleDelete={() => handleDelete(item.id)}
                                    />
                                </div>
                            </div>
                        )
                    })
                }
                <TableMsg isError={isError} data={data} isLoading={isLoading} isDeleting={isDeleting} />
            </div >
        </div>
    )
}

function ActionBtn({ id, showMenu, item, handleDelete }) {
    const actionBtn = [
        { icon: Eye, to: { pathname: `${ViewBookDetail}/${id}`, state: { data: item, type: 'view', from: 'list', showMenu } } },
        { icon: Pencil, to: { pathname: `${UpdateBook}/${id}`, state: { data: item, type: 'update', from: 'list', showMenu } } },
    ]

    return (
        <div className="content-item action">
            {actionBtn.map((item, index) => {
                return (
                    <Link key={index} to={item.to} className="link-icon">
                        <img className={index === 0 ? "action-icon eye" : "action-icon"} src={item.icon} />
                    </Link>
                )
            })}
            <button className="btn-icon" onClick={handleDelete}>
                <img className="action-icon" src={Trash} />
            </button>
        </div>
    )
}

function TableMsg({ isError, data, isDeleting }) {
    return (
        isError || !data.length || isDeleting ?
            <div className={isError ? "table-msg warn" : "table-msg"}>
                <p>{
                    isError ? "Failed request to server" :
                        !data.length ? "No data yet" :
                            isDeleting ? "Refreshing data..." :
                                null
                }</p>
            </div>
            : null

    )
}

export default Table