import './index.css';
import { Link } from 'react-router-dom';
function Modal({ visible, type, style, successContent = {}, failedContent = {}, btnLeft = {}, btnRight = {}, src, close }) {
    const data = style === 'success' ? successContent : failedContent
    return (
        visible &&
        <div className={type !== 'img' ? "modal-container" : "modal-container img"}>
            <div className={type !== 'img' ? "modal-content" : "modal-content-img"}>
                {type === 'info' || type === 'confirm' ?
                    <>
                        <div className="modal-icon-msg">
                            <img className="modal-icon" src={data.icon} />
                            <p className="modal-title">{data.title}</p>
                            <p className="modal-msg">{data.msg}</p>
                        </div>
                        <div className="modal-btn">
                            {type === 'info' ?
                                <Link className={style === 'success' ? "btn-left" : "btn-left failed"} to={btnLeft.to}>{btnLeft.item}</Link> :
                                <button className={style === 'success' ? "btn-right" : "btn-right failed"} onClick={btnLeft.to}>{btnLeft.item}</button>
                            }
                            <button className={style === 'success' ? "btn-right" : "btn-right failed"} onClick={btnRight.to}>{btnRight.item}</button>
                        </div>
                    </> :
                    <>
                        <button className="btn-close-modal" onClick={close}>X</button>
                        <img className="modal-img" src={src} />
                    </>
                }
            </div>
        </div>
    )
}

export default Modal