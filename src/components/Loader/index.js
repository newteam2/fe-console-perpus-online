import './index.css';

function Loader(props) {
    const { isLoading, showMenu } = props
    return (
        isLoading &&
        <div className={showMenu ? "loader-container active" : "loader-container"}>
            <div className="loader" />
        </div>
    )
}

export default Loader