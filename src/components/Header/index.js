import React from 'react';
import { Link } from 'react-router-dom';
import './index.css';

function Header({ showMenu, headerTitle, rightBtn = false, btnData = {} }) {
    const { to, title, icon } = btnData
    return (
        <div className="header-container">
            <div className={showMenu ? "res-header-container active" : "res-header-container"}>
                <div className="header-content">
                    <div className="header-title-container">
                        <p className="header-title">{headerTitle}</p>
                    </div>
                </div>
                <div className="header-content right">
                    {rightBtn &&
                        <Link
                            className="link-add"
                            to={to}
                        >
                            <p className="btn-title-add">{title}</p>
                            <div className="btn-icon-add">
                                <img className="icon-add" src={icon} />
                            </div>
                        </Link>
                    }
                </div>

            </div>
        </div>
    )
}

export default Header