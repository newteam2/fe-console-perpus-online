import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { ListBook } from '../../config/routes';
import './index.css';
import { Logo, Menu } from '../../config/images';

function SideNav(props) {
    const [active, setActive] = useState(0)
    const history = useHistory();
    const { showMenu, clickMenu } = props
    const listMenu = [
        { id: 0, to: { pathname: ListBook, state: { active, showMenu } }, name: 'Book' },
        { id: 1, to: { pathname: "", state: { active, showMenu } }, name: 'Publisher' },
        { id: 2, to: { pathname: "", state: { active, showMenu } }, name: 'Shop' }
    ]

    const handleClick = (id, to) => {
        setActive(id)
        if (id === 0) history.push(to)
    }

    return (
        <div className={showMenu ? "side-nav active" : "side-nav"}>
            <div className="menu-container">
                <div className="top-menu" />
                <div className="main-menu">
                    <div className="logo">
                        <img className="logo-img" src={Logo} />
                    </div>
                    <div className="menu">
                        <ul>
                            {listMenu.map((item, index) => {
                                return (
                                    <li key={index}>
                                        <button
                                            className={active == item.id ? "link-menu active" : "link-menu"}
                                            onClick={() => handleClick(item.id, item.to)}
                                        >
                                            {item.name}
                                        </button>
                                    </li>
                                )
                            })}
                        </ul>
                    </div>
                </div>
            </div>
            <div className="burger-container">
                <button className={showMenu ? "burger active" : "burger"} onClick={() => clickMenu()}>
                    <img className="burger-icon" src={Menu} />
                </button>
            </div>
        </div>
    )
}

export default SideNav