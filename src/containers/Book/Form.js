import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useHistory } from 'react-router-dom';
import { addBookAction, updateBookAction, uploadImageAction, uploadFileAction } from '../../actions/bookAction';
import SideNav from '../../components/SideNav';
import { ListBook, ViewBookDetail, UpdateBook } from '../../config/routes';
import Header from '../../components/Header';
import './Form.css';
import Modal from '../../components/Modal';
import ModalImage from '../../components/Modal';
import ModalConfirm from '../../components/Modal';
import { Like, DisLike } from '../../config/images';
import moment from 'moment';

function Form({ location }) {
    const propsMenu = location.state && location.state.showMenu
    const { type, data, from } = location.state && location.state
    const book = useSelector((state) => state.book)
    const {
        urlImage,
        urlFile,
        isLoadingUpImg,
        isSuccessUpImg,
        isErrorUpImg,
        isLoadingUpFile,
        isSuccessUpFile,
        isErrorUpFile,
        isLoading,
        isSuccessAdd,
        isErrorAdd,
        isSuccessUpdate,
        isErrorUpdate,
        bookDetail
    } = book
    let item = data

    const [showMenu, setShowMenu] = useState(propsMenu)
    const [isbn, setIsbn] = useState(type !== 'add' ? item.book_isbn : "")
    const [title, setTitle] = useState(type !== 'add' ? item.book_name : "")
    const [description, setDesc] = useState(type !== 'add' ? item.book_description : "")
    const [publisher, setPublisher] = useState(type !== 'add' ? item.publisher : "")
    const [publishDate, setPublishDate] = useState(type !== 'add' ? moment(item.published_date).format("YYYY-MM-DD") : "")
    const [image, setImage] = useState({ name: type !== 'add' ? item.book_image : '', path: '', url: '', data: '' })
    const [file, setFile] = useState({ name: type !== 'add' ? item.book_content : '', path: '', url: '', data: '' })
    const [typeUpload, setTypeUpload] = useState("")
    const [validate, setValidate] = useState({
        isbn: false,
        title: false,
        publisher: false,
        publishDate: false
    })
    const [modal, setModal] = useState(false)
    const [modalImg, setModalImg] = useState(false)
    const [copyImg, setCopyImg] = useState(false)
    const [copyFile, setCopyFile] = useState(false)
    const [validateImg, setValidateImg] = useState(false)
    const [validateFile, setValidateFile] = useState(false)
    const [backConfirm, setBackConfirm] = useState(false)
    const dispatch = useDispatch()
    const history = useHistory()

    // Function for valdate form and add data to server
    const handleSubmitValue = (type) => {
        let validateForm = {}
        const setObject = (field, value) => {
            validateForm = { ...validateForm, [field]: value }
        }

        if (isbn.length < 7) setObject('isbn', true)
        else setObject('isbn', false)

        if (!title) setObject('title', true)
        else setObject('title', false)

        if (!publisher) setObject('publisher', true)
        else setObject('publisher', false)

        if (!publishDate) setObject('publishDate', true)
        else setObject('publishDate', false)

        if (isbn.length === 7 && title && publisher && publishDate) {
            const body = {
                isbn,
                title,
                description,
                publisher,
                published_date: publishDate,
                image: type === 'add' ? image.url : image.path ? image.url : image.name,
                content: type === 'add' ? file.url : file.path ? file.url : file.name,
            }
            type === 'add' ?
                dispatch(addBookAction(body)) :
                dispatch(updateBookAction({ id: item.id, body }))
        }
        setValidate(validateForm)
    }

    // Function for preparing data before upload image or file
    const handleChooseFile = ({ data, type }) => {
        setTypeUpload(type)
        const upload_file = data.files[0]
        const formData = new FormData();
        const typeFile = data.files[0].type
        const stateFileTrue = { name: data.files[0].name, path: data.value, url: '', data: formData }
        const stateFileFalse = { name: 'Incorrect file type', path: '', url: '', data: '' }
        if (type === 'image') {
            formData.append('picture', upload_file)
            setImage(typeFile.includes("image") ? stateFileTrue : stateFileFalse)
        }
        else if (type === 'file') {
            formData.append('document', upload_file)
            setFile(typeFile.includes("pdf") || typeFile.includes("msword") ? stateFileTrue : stateFileFalse)
        }
    }

    // Function for reset form after success submit
    const resetForm = (value) => {
        if (value === 'add') {
            setIsbn("")
            setTitle("")
            setDesc("")
            setPublisher("")
            setPublishDate("")
            setImage({ name: '', path: '', url: '', data: '' })
            setFile({ name: '', path: '', url: '', data: '' })
            setValidateImg(false)
            setValidateFile(false)
        } else if (value === 'view') {
            setIsbn(type !== 'add' ? item.book_isbn : "")
            setTitle(type !== 'add' ? item.book_name : "")
            setDesc(type !== 'add' ? item.book_description : "")
            setPublisher(type !== 'add' ? item.publisher : "")
            setPublishDate(type !== 'add' ? moment(item.published_date).format("YYYY-MM-DD") : "")
            setImage({ name: type !== 'add' ? item.book_image : '', path: '', url: '', data: '' })
            setFile({ name: type !== 'add' ? item.book_content : '', path: '', url: '', data: '' })
            setValidateImg(false)
            setValidateFile(false)
        }
    }

    // Function for copy text to clipboard
    const copyToClipboard = async (type, text) => {
        try {
            await navigator.clipboard.writeText(text)
            if (type === 'img') {
                setCopyImg(true)
                setCopyFile(false)
            } else {
                setCopyFile(true)
                setCopyImg(false)
            }
        } catch (err) {
            type === 'img' ? setCopyImg(false) : setCopyFile(false)
        }
    }

    // Function for handle cancel button
    const handleClickCancel = () => {
        let validateAdd = false
        let validateUpdate = false
        if (type === 'add') {
            validateAdd =
                isbn.length ||
                title ||
                description ||
                publisher ||
                publishDate ||
                image.url ||
                file.url
        } else if (type === 'update') {
            const data = isSuccessUpdate ? bookDetail : item
            validateUpdate =
                isbn !== data.book_isbn ||
                title !== data.book_name ||
                description !== data.book_description ||
                publisher !== data.publisher ||
                publishDate !== moment(data.published_date).format("YYYY-MM-DD") ||
                image.name !== data.book_image ||
                file.name !== data.book_content
        }
        const validateType = type === 'add' ? validateAdd : type === 'update' ? validateUpdate : false

        if (validateType) {
            setBackConfirm(true)
        } else {
            history.push({
                pathname: from === 'view' ? `${ViewBookDetail}/${item && item.id}` : ListBook,
                state: from === 'list' ? { showMenu } : { showMenu, data: isSuccessUpdate && bookDetail ? bookDetail : item, type: 'view', from: 'update' }
            })
        }
    }

    // Function for handle click discard on upload file
    const handleDiscardFile = (value) => {
        if (type === 'add') {
            if (value === 'img')
                setImage({ name: '', path: '', url: '', data: '' })
            else
                setFile({ name: '', path: '', url: '', data: '' })
        } else if (type === 'update') {
            if (value === 'img') {
                if (image.name !== item.book_image)
                    setImage({ name: type !== 'add' ? item.book_image : '', path: '', url: '', data: '' })
                else
                    setImage({ name: '', path: '', url: '', data: '' })
            } else {
                if (file.name !== item.book_content)
                    setFile({ name: type !== 'add' ? item.book_content : '', path: '', url: '', data: '' })
                else
                    setFile({ name: '', path: '', url: '', data: '' })
            }
        }
    }

    // Function for upload image or file to server
    useEffect(() => {
        if (typeUpload === 'image' && image.path && !image.url) {
            dispatch(uploadImageAction(image.data))
            setValidateImg(true)
        } else if (typeUpload === 'file' && file.path && !file.url) {
            dispatch(uploadFileAction(file.data))
            setValidateFile(true)
        }
    }, [image, file])

    // Function for set state url image or file after upload to server
    useEffect(() => {
        if (typeUpload === 'image') setImage({ ...image, url: urlImage })
        else if (typeUpload === 'file') setFile({ ...file, url: urlFile })
    }, [urlImage, urlFile])

    // Function for auto run resetForm and open modal after success submit
    useEffect(() => {
        if (isSuccessAdd || isSuccessUpdate) {
            isSuccessAdd && resetForm('add')
            setModal(true)
        }
        if (isErrorAdd || isErrorUpdate) setModal(true)
    }, [isSuccessAdd, isErrorAdd, isSuccessUpdate, isErrorUpdate])

    // Function for reset form view
    useEffect(() => {
        if (isSuccessUpdate && bookDetail) resetForm('view')
        return () => {
            resetForm('view')
            setModal(false)
        }
    }, [type, from])

    return (
        <div className="form-book-container">
            <SideNav showMenu={showMenu} clickMenu={() => setShowMenu(!showMenu)} />
            <Modal
                visible={modal}
                type="info"
                style={isSuccessAdd || isSuccessUpdate ? "success" : "failed"}
                successContent={{
                    icon: Like,
                    title: 'Success',
                    msg: type === 'add' ? 'Your book has been successfully added to the database, you can view your book in the Book List' : 'Your book has been successfully updated'
                }}
                failedContent={{
                    icon: DisLike,
                    title: 'Failed',
                    msg: type === 'add' ? 'Your book failed to add to the database' : 'Your book failed to update'
                }}
                btnLeft={{
                    item: from === 'list' ? (isSuccessAdd || isSuccessUpdate ? 'Check on book list' : 'Back to book list') : 'Check on book detail',
                    to: {
                        pathname: from === 'view' ? `${ViewBookDetail}/${item && item.id}` : ListBook,
                        state: from === 'list' ? { showMenu } : { showMenu, data: isSuccessUpdate && bookDetail ? bookDetail : item, type: 'view', from: 'update' }
                    }
                }}
                btnRight={{
                    item: isSuccessAdd || isSuccessUpdate ? 'Okay' : 'Try again',
                    to: () => setModal(false)
                }}
            />
            <ModalImage
                visible={modalImg}
                type="img"
                src={image.name}
                close={() => setModalImg(false)}
            />
            <ModalConfirm
                visible={backConfirm}
                type="confirm"
                style="failed"
                failedContent={{
                    icon: DisLike,
                    title: 'Cancel :(',
                    msg: 'Are you sure you want to cancel & discard all changes in the form ?'
                }}
                btnLeft={{
                    item: 'No',
                    to: () => setBackConfirm(false)
                }}
                btnRight={{
                    item: 'Yes',
                    to: () => {
                        history.push({
                            pathname: from === 'view' ? `${ViewBookDetail}/${item && item.id}` : ListBook,
                            state: from === 'list' ? { showMenu } : { showMenu, data: isSuccessUpdate && bookDetail ? bookDetail : item, type: 'view', from: 'update' }
                        })
                        setBackConfirm(false)
                    }
                }}
            />
            <div className={showMenu ? "content active" : "content"}>
                <Header
                    showMenu={showMenu}
                    headerTitle={type === 'add' ? 'Add Book' : type === 'view' ? 'Book Detail' : 'Update Book'}
                />
                <div className="form-main-content">
                    <div className={showMenu ? "form-container active" : "form-container"}>
                        <div className="form-body">
                            <div className="form-row">
                                <label className="form-label">ISBN</label>
                                <input
                                    className={validate.isbn ? "form-input validate" : "form-input"}
                                    value={isbn}
                                    type="number"
                                    onInput={(e) => e.target.value = e.target.value.slice(0, 7)}
                                    placeholder="ISBN"
                                    onChange={(e) => setIsbn(e.target.value)}
                                    readOnly={type === 'view' ? true : false}
                                />
                                <FormValidate type="input" validate={validate.isbn} msg="Must be seven digits" />
                            </div>
                            <div className="form-row">
                                <label className="form-label">Title</label>
                                <input
                                    className={validate.title ? "form-input validate" : "form-input"}
                                    value={title}
                                    type="text"
                                    placeholder="Title"
                                    onChange={(e) => setTitle(e.target.value)}
                                    readOnly={type === 'view' ? true : false}
                                />
                                <FormValidate type="input" validate={validate.title} msg="Title is required" />
                            </div>
                            <div className="form-row desc">
                                <label className="form-label">Description</label>
                                <textarea
                                    className="form-input desc"
                                    value={description}
                                    placeholder="Description"
                                    onChange={(e) => setDesc(e.target.value)}
                                    readOnly={type === 'view' ? true : false}
                                />
                            </div>
                            <div className="form-row">
                                <label className="form-label">Publisher</label>
                                <input
                                    className={validate.publisher ? "form-input validate" : "form-input"}
                                    value={publisher}
                                    type="text"
                                    placeholder="Publisher"
                                    onChange={(e) => setPublisher(e.target.value)}
                                    readOnly={type === 'view' ? true : false}
                                />
                                <FormValidate type="input" validate={validate.publisher} msg="Publisher is required" />
                            </div>
                            <div className="form-row">
                                <label className="form-label">Publish Date</label>
                                <input
                                    className={validate.publishDate ? "form-input validate" : "form-input"}
                                    value={publishDate}
                                    type="date"
                                    placeholder="Publish Date"
                                    onChange={(e) => setPublishDate(e.target.value)}
                                    readOnly={type === 'view' ? true : false}
                                />
                                <FormValidate type="input" validate={validate.publishDate} msg="Publish date is required" />
                            </div>
                            <div className="form-row">
                                <label className="form-label">Cover/Image</label>
                                <div className="form-input file">
                                    {type === 'add' || type === 'update' ?
                                        <>
                                            <div className="btn-label">
                                                <label className="label-btn-upload">Choose image
                                                    <input
                                                        className="form-input-file"
                                                        type="file"
                                                        onChange={e => handleChooseFile({ data: e.target, type: 'image' })}
                                                        onClick={e => e.target.value = null}
                                                        readOnly={type === 'view' ? true : false}
                                                    />
                                                </label>
                                                <label className={image.name ? "file-name" : "file-name failed"}>{image.name}</label>
                                            </div>
                                            {image.name && isSuccessUpImg ?
                                                <button className="btn-copy" onClick={() => handleDiscardFile('img')}>
                                                    <p className="req-type">Discard</p>
                                                </button> :
                                                type === 'update' && image.name && image.name === item.book_image ?
                                                    <button className="btn-copy" onClick={() => handleDiscardFile('img')}>
                                                        <p className="req-type">Discard</p>
                                                    </button> :
                                                    <label className="req-type">*.png/.img</label>
                                            }
                                        </> :
                                        <>
                                            <div className="btn-label">
                                                <button className="btn-show-img" onClick={() => setModalImg(true)}>
                                                    <label className="label-btn-upload">Show image</label>
                                                </button>
                                                <label className="file-name">{image.name ? image.name : 'No photo'}</label>
                                            </div>
                                            <button className="btn-copy" onClick={() => copyToClipboard('img', image.name)} disabled={!image.name}>
                                                <p className="req-type">{copyImg ? 'Copied' : 'Copy'}</p>
                                            </button>
                                        </>
                                    }
                                </div>
                                <FormValidate type="file" form={type} show={validateImg && image.name} isLoading={isLoadingUpImg} isSuccess={isSuccessUpImg} isError={isErrorUpImg} />
                            </div>
                            <div className="form-row">
                                <label className="form-label">File</label>
                                <div className="form-input file">
                                    {type === 'add' || type === 'update' ?
                                        <>
                                            <div className="btn-label">
                                                <label className="label-btn-upload">Choose file
                                            <input
                                                        className="form-input-file"
                                                        type="file"
                                                        onChange={(e) => handleChooseFile({ data: e.target, type: 'file' })}
                                                        onClick={e => e.target.value = null}
                                                        readOnly={type === 'view' ? true : false}
                                                    />
                                                </label>
                                                <label className={file.name ? "file-name" : "file-name failed"}>{file.name}</label>
                                            </div>
                                            {file.name && isSuccessUpFile ?
                                                <button className="btn-copy" onClick={() => handleDiscardFile('file')}>
                                                    <p className="req-type">Discard</p>
                                                </button> :
                                                type === 'update' && file.name && file.name === item.book_content ?
                                                    <button className="btn-copy" onClick={() => handleDiscardFile('file')}>
                                                        <p className="req-type">Discard</p>
                                                    </button> :
                                                    <label className="req-type">*.pdf/.doc</label>
                                            }
                                        </> :
                                        <>
                                            <div className="btn-label">
                                                <a className="btn-show-img" href={file.name} target="_blank" rel="noopener noreferrer">
                                                    <label className="label-btn-upload">Download file</label>
                                                </a>
                                                <label className="file-name">{file.name ? file.name : 'No file'}</label>
                                            </div>
                                            <button className="btn-copy" onClick={() => copyToClipboard('file', file.name)} disabled={!file.name}>
                                                <p className="req-type">{copyFile ? 'Copied' : 'Copy'}</p>
                                            </button>
                                        </>
                                    }
                                </div>
                                <FormValidate type="file" form={type} show={validateFile && file.name} isLoading={isLoadingUpFile} isSuccess={isSuccessUpFile} isError={isErrorUpFile} />
                            </div>
                        </div>
                        <div className="form-footer">
                            <div className="form-btn">
                                <button className="btn-footer" onClick={handleClickCancel} disabled={isLoading}>{type === 'view' ? 'Back' : 'Cancel'}</button>
                                {type === 'view' ?
                                    <Link
                                        className="btn-footer submit"
                                        to={{
                                            pathname: `${UpdateBook}/${item.id}`,
                                            state: { data: item, type: 'update', from: 'view', showMenu }
                                        }}
                                    >
                                        Update
                                    </Link> :
                                    <button className="btn-footer submit" onClick={() => handleSubmitValue(type)} disabled={isLoading}>Submit</button>
                                }
                            </div>
                            <FormValidate type="submit" isLoading={isLoading} show={true} form={type} />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

function FormValidate({ type, form, show, isLoading, isSuccess, isError, validate, msg }) {
    if (type === 'file' || type === 'submit') {
        if (form !== 'view' && show) {
            return (
                <div className="form-validate">
                    {isLoading ?
                        <div className="up-loader" /> :
                        isSuccess ?
                            <p className="up-msg">{type === 'file' ? "Upload" : "Submit"} successful</p> :
                            isError ?
                                <p className="up-msg failed">{type === 'file' ? "Upload" : "Submit"} failed</p> :
                                null}
                </div>
            )
        } else return null
    } else if (type === 'input') {
        return (
            validate &&
            <div className="form-validate">
                <p className="up-msg failed">* {msg}</p>
            </div>
        )
    }
}

export default Form