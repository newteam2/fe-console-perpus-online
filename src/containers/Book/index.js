import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AddNewBook } from '../../config/routes';
import { getListBookAction, deleteBookAction } from '../../actions/bookAction';
import './index.css';
import SideNav from '../../components/SideNav';
import TableBook from '../../components/Table/Book';
import Header from '../../components/Header';
import Modal from '../../components/Modal';
import { BackgroundView, Add, DisLike } from '../../config/images';

function Book({ location }) {
    const propsMenu = location.state ? location.state.showMenu : true
    const [showMenu, setShowMenu] = useState(propsMenu)
    const [modal, setModal] = useState(false)
    const [bookId, setBookId] = useState(null)
    const book = useSelector((state) => state.book)
    const { isLoading, list, isError, fetchDelete, isDeleting } = book
    const dispatch = useDispatch()

    const handleDeleteBook = (id) => {
        setModal(true)
        setBookId(id)
    }

    useEffect(() => {
        dispatch(getListBookAction())
    }, [dispatch, fetchDelete])

    return (
        <div className="book-container" style={{
            backgroundImage: `url(${BackgroundView})`,
            height: '450px',
        }}>
            <SideNav showMenu={showMenu} clickMenu={() => setShowMenu(!showMenu)} />
            <Modal
                visible={modal}
                type="confirm"
                style="failed"
                failedContent={{
                    icon: DisLike,
                    title: 'Delete :(',
                    msg: 'Are you sure you want to delete this book ?'
                }}
                btnLeft={{
                    item: 'No',
                    to: () => setModal(false)
                }}
                btnRight={{
                    item: 'Yes',
                    to: () => {
                        dispatch(deleteBookAction(bookId))
                        setModal(false)
                    }
                }}
            />
            <div className={showMenu ? "book-content active" : "book-content"}>
                <Header
                    showMenu={showMenu}
                    headerTitle="List Book"
                    rightBtn={true}
                    btnData={{
                        to: {
                            pathname: AddNewBook,
                            state: { showMenu, type: 'add', from: 'list' }
                        },
                        title: 'Add book',
                        icon: Add
                    }}
                />
                <TableBook
                    showMenu={showMenu}
                    data={list}
                    isLoading={isLoading}
                    isError={isError}
                    isDeleting={isDeleting}
                    handleDelete={handleDeleteBook}
                />
            </div>
        </div>
    )
}

export default Book