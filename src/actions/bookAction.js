import { GetListBook, AddBook, DeleteBook, UploadImage, UploadFile, UpdateBook } from '../config/actionType';

export const getListBookAction = () => {
    return {
        type: GetListBook,
    }
}

export const addBookAction = (value) => {
    return {
        type: AddBook,
        value
    }
}

export const updateBookAction = (value) => {
    return {
        type: UpdateBook,
        value
    }
}

export const deleteBookAction = (value) => {
    return {
        type: DeleteBook,
        value
    }
}

export const uploadImageAction = (value) => {
    return {
        type: UploadImage,
        value
    }
}

export const uploadFileAction = (value) => {
    return {
        type: UploadFile,
        value
    }
}