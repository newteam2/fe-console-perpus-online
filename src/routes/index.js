import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
// Config routes
import {
    ListBook,
    ViewBookDetailId,
    AddNewBook,
    UpdateBookId
} from '../config/routes';
// Book
import Book from '../containers/Book';
import FormBook from '../containers/Book/Form';

function Routes() {
    return (
        <Router>
            <Switch>
                <Route exact path={ListBook} component={Book} />
                <Route path={ViewBookDetailId} component={FormBook} />
                <Route path={AddNewBook} component={FormBook} />
                <Route path={UpdateBookId} component={FormBook} />
            </Switch>
        </Router>
    );
};
export default Routes;
